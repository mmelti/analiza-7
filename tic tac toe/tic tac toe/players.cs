﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tic_tac_toe
{
    public partial class players : Form
    {
        public players()
        {
            InitializeComponent();
        }

        private void playbtn_Click(object sender, EventArgs e)
        {
            Form1.setplayer(t1.Text, t2.Text);
            this.Close();
        }
    }
}

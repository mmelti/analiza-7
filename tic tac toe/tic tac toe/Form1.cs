﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tic_tac_toe
{
    public partial class Form1 : Form
    {
        bool turn = true;
        int tcounter = 0;
        int xwins = 0;
        int owins = 0;
        static String p1, p2;

        public Form1()
        {
            InitializeComponent();
        }

        public static void setplayer(String n1,String n2)
        {
            p1 = n1;
            p2 = n2;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            players p = new players();
            p.ShowDialog();
            player1.Text = p1;
            player2.Text = p2;
            xwins = 0;
            owins = 0;
            player1wins.Text = xwins.ToString();
            player2wins.Text = owins.ToString();
            whoturn.Text = (p1 + " turn");
        }

        private void btn_clk(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
            {
                b.Text = "X";
                whoturn.Text = (p2 + " turn");
            }
            else
            {
                b.Text = "O";
                whoturn.Text = (p1 + " turn");
            }
            turn = !turn;
            b.Enabled = false;

            tcounter++;
            checkWin();
        }

        private void checkWin()
        {
            bool winner = false;
            if ((a1.Text == a2.Text) && (a2.Text == a3.Text) && !a1.Enabled)
                winner = true;
            else if ((b1.Text == b2.Text) && (b2.Text == b3.Text) && !b1.Enabled)
                winner = true;
            else if ((c1.Text == c2.Text) && (c2.Text == c3.Text) && !c1.Enabled)
                winner = true;

            else if ((a1.Text == b1.Text) && (b1.Text == c1.Text) && !a1.Enabled)
                winner = true;
            else if ((a2.Text == b2.Text) && (b2.Text == c2.Text) && !a2.Enabled)
                winner = true;
            else if ((a3.Text == b3.Text) && (b3.Text == c3.Text) && !a3.Enabled)
                winner = true;

            else if ((a1.Text == b2.Text) && (b2.Text == c3.Text) && !a1.Enabled)
                winner = true;
            else if ((a3.Text == b2.Text) && (b2.Text == c1.Text) && !c1.Enabled)
                winner = true;

            if (winner)
            {
                string whowon = " ";
                if (!turn)
                {
                    whowon = p1;
                    owins++;
                    player1wins.Text = owins.ToString();
                }
                else
                {
                    whowon = p2;
                    xwins++;
                    player2wins.Text = xwins.ToString();
                }
                MessageBox.Show(whowon+ " wins");
            }
            else if (tcounter == 9)
            {
                MessageBox.Show("It's a draw");
            }

        }

        private void disablebtn()
        {
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
            catch { };
        }

        private void newbtn_Click(object sender, EventArgs e)
        {
            turn = false;
            tcounter = 0;
           
                foreach (Control c in Controls)
            { try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }catch { };
            }
            newplay.Text = ("New players");
            newbtn.Text = ("New game");
        }
    }
}
